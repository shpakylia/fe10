class Component {
    _element = null;

    constructor(props){
        this._props = props;
        this._initialize();
        this._createLayout();
    }
    appendTo(container){
        container.append(this._element);
    }
    _createLayout(){

    }
    _initialize(){

    }
    destroy(){
        this._element.remove();
    };
}

class TodoForm extends Component{
    _onKeyDown = (event)=>{
        if(this._element.value && event.key === 'Enter'){
            this._props.addItemFn(this._element.value);
            this._element.value = '';
        }
    };
    constructor(addItemFn){
        super({addItemFn});
        this._listenAdd();
    }

    _createLayout(){
        this._element = document.createElement('input');
    }
    _listenAdd(){

        this._element.addEventListener('keydown', this._onKeyDown );
    }
    destroy(){
        this._element.removeEventListener('keydown', this._onKeyDown);
        super.destroy();
    }


}

class TodoItem extends Component{
    _onCheckboxChange = event => {
        this._props.onCompleteChanged(event.target.checked);
    };
    _onRemoveBtnClick = ()=>{this._props.onRemove()};
    constructor(name, onCompleteChanged, onRemove){
        super({
            name,
            onCompleteChanged,
            onRemove
        });
        this._listenRemove();
        this._listenCompleteChange();

    }
    _listenRemove(){
        this._removeBtn.addEventListener('click', ()=>{this._props.onRemove()})
    }
    _listenCompleteChange(){
        this._checkbox.addEventListener('change', this._onCheckboxChange)

    }
    _createLayout(){
        const container = document.createElement('div');
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        const nameEl = document.createElement('span');
        nameEl.innerText = this._props.name;
        const removeBtn = document.createElement('button');
        removeBtn.innerText = 'x';
        container.append(checkbox);
        container.append(nameEl);
        container.append(removeBtn);

        this._element = container;
        this._removeBtn = removeBtn;
        this._checkbox = checkbox;
    }
    destroy(){
        this._removeBtn.removeEventListener('click', this._onRemoveBtnClick);
        this._checkbox.addEventListener('change', this._onCheckboxChange);
    }
}

class TodoList extends Component{
    _items = [];
    constructor(){
        super();
    }

    _createLayout(){
        const listEl = document.createElement('ul');
        this._element = listEl;
    }
    _onItemRemove(item){
        const {container} = this._items.find(({item: it}) => it===item);
        container.remove();
        item.destroy();
    }
    add(name){
        //add item
        const item = new TodoItem(name, ()=>{}, ()=>{
            this._onItemRemove(item);
        });
        const itemContainer = document.createElement('li');
        item.appendTo(itemContainer);
        this._element.append(itemContainer);
        this._items.push({container: itemContainer, item});

    }
    clearAll(){
        this._items.forEach(({container, item})=>{
            container.remove();
            item.destroy();
        })
    }
}
class TodoApp extends Component{
    constructor(){
        super();
        console.log(this._list);
    }

    _initialize(){
        this._list = new TodoList();
        this._form = new TodoForm(name=> this._onAddItenName(name));

    }
    _onAddItenName(name){
        this._list.add(name);
    }
    _createLayout(){
        this._element = document.createElement('div');
        this._form.appendTo(document.body);
        this._list.appendTo(document.body);
    }

    destroy(){
        this._list.destroy();
        this._form.destroy();
        super.destroy();
    }

}
// const form = new TodoForm(onAddItem);
// form.appendTo(document.body);
//
// function onAddItem(name) {
//     list.add(name)
// }

// const item = new TodoItem('sds', onCompleteChanged, onRemove);
// item.appendTo(document.body);

// function onCompleteChanged(isCompleted) {
//     console.log(`Complete changed: ${isCompleted}`);
// }
//
// function onRemove() {
//     console.log('its remove');
// }
//

// const list = new TodoList();
//
// list.add('Name');
// list.appendTo(document.body);
const app = new TodoApp();
