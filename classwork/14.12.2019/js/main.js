// Array(5).fill(0).map((el, i)=> {
//     console.log(i);
// })
//

class Point {
    constructor(x, y){
        this.x = x;
        this.y = y;
    }
}

class Path {
    /**
     * @return {number}
     */
    getDistance(){

    }

    getStartingPoint(){

    }
}
 class Line extends Path{

    constructor(start, end){
        super()
        this._start = start;
        this._end = end;
    }

    getStartingPoint(){
        return this._start;
    }

    getDistance() {
        let dx = this._end.x - this._start.x;
        let dy = this._end.x - this._start.x;

        return Math.hypot(dx, dy);
    }

 }


 class PolyLine extends Path{
     /**
      *
      * @param points {Point[]}
      */
    constructor(points){
        super();
        this._points = points;
         this._lines = Array.from({length:this._points.length -1}, (el, i)=> {return new Line(this._points[i], this._points[i+1])});
    }
     getStartingPoint(){
         return this._points[0];
     }

     getDistance() {
         console.log(this._lines);
         return this._lines.reduce((sum, line)=> {return sum + line.getDistance();}, 0);
     }

 }

 const linePath = new Line(new Point(3,4), new Point(6,8));
 const polylinePath = new PolyLine([new Point(3,4), new Point(6,8), new Point(10,20)]);
console.log(linePath.getDistance());
console.log(polylinePath.getDistance());