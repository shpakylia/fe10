class Score {
    score = 0;
    constructor(){
        this._createLoyout();
    }

    increment(){
        this.score++;
    }
    reset(){
        this.score = 0;
    }

    appendTo(element){
        element.append(this._container);
    }

    _createLoyout(){
        this._container = document.createElement('span');
        this._updateLoyot();
    }

    _updateLoyot(){
        this._container.innerHTML = `Score: ${this.score}`;
    }

}

class Game {
    _container;
    _score;
    _randomizer;
    _buttonContainer;
    _currentKey;
    _keyWords = ['up', 'down', 'left', 'right'];
    constructor() {
        this._score = new Score();
        this._randomizer = new Randomizer();
        this._createLoyout();

        this._container.addEventListener('keydown', event=>{

        })
    }


    _createLoyout(){
        const container = document.createElement('div');
        const buttonName = document.createElement('div');
        const scoreContainer = document.createElement('div');
        container.append(buttonName);
        container.append(scoreContainer);

        this._buttonContainer = buttonName;
        this._score.appendTo(scoreContainer);
        this._container = container;
    }

    appendTo(element){
        element.append(this._container);
    }

    _randomizeBtn(){
        this._currentKey = this._randomizer.randomize();
        this._buttonContainer.innerHTML = this._keyWords[this._currentKey];
    }


}

class Randomizer {

    randomize(){
        return Math.floor(Math.random()*4);
    }
}


const game = new Game();
game.appendTo(document.body);

