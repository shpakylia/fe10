const clients = (function () {
    const clientsArr = [
        {name: 'Tolia'}
    ];

    return {
        addClient: function (client= {name: 'newClient'}) {
            clientsArr.push(client);
            return clientsArr.length-1;
        },
        removeClient: function (number) {
            clientsArr.splice(number, 1);
            return clientsArr;
        },
        getList: function () {
            return clientsArr;
        }
    }
})();