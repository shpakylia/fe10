const employes = (function () {
    const employesArr = [
        {name: 'Tolia'}
    ];

    return {
        addEmploye: function (client= {name: 'newEmploye'}) {
            employesArr.push(client);
            return employesArr.length-1;
        },
        removeEmploye: function (number) {
            employesArr.splice(number, 1);
            return employesArr;
        },
        getList: function () {
            return employesArr;
        }
    }
})();