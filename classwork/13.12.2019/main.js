document.addEventListener('DOMContentLoaded', function () {
    let clientsBlock = document.querySelector('.clients-items');
    let employesBlock = document.querySelector('.employes-items');
    function print(list, type='client') {
        let block = type=='client' ? clientsBlock : employesBlock;
        block.innerHTML = '';
        list.getList().forEach((val, key)=>{
            block.innerHTML += `<div><span>${val.name}<span><button data-${type}="${key}" class="btn btn-primary delete-${type}">Delete</button></div>`;
        });
        let delElems = document.querySelectorAll(`.delete-${type}`);
        console.log([...delElems]);
        [...delElems].forEach((el)=>{
            console.log(el);
            el.addEventListener('click', onDel.bind(this, type))
        });

    }
    print(clients);
    print(employes, 'employe');


    function onDel(type, event) {
        console.log('click', type, event);
        event.preventDefault();
        const el = event.target;
        switch (type) {
            case 'client': {
                const id = el.dataset.type;
                clients.removeClient(id);
                print(clients);
                break;
            }
            case 'employe': {
                const id = el.dataset.type;
                employes.removeEmploye(id);
                print(employes, 'employe');
                break;
            }
            default:{
                const id = el.dataset.type;
                clients.removeClient(id);
                print(clients);
            }

        }
    }



    function add(type) {
        console.log(type);
        switch (type) {
            case 'client':{
                const inputClient = prompt('Enter client Name', 'Julia');
                if(!inputClient) return;
                const newClient = {name: inputClient};
                newClient.id = clients.addClient(newClient);
                print(clients);
                break;
            }
            case 'employe':{
                const inputEmploye = prompt('Enter employe Name', 'Julia');
                if(!inputEmploye) return;
                const newEmploye = {name: inputEmploye};
                newEmploye.id = employes.addEmploye(newEmploye);
                print(employes, 'employe');
                break;
            }
        }

    }
    document.querySelector('.add-client').addEventListener('click', add.bind(this, 'client'));
    document.querySelector('.add-employe').addEventListener('click', add.bind(this, 'employe'));

})
